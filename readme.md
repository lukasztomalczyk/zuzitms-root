How to connect ssh to server develop:
    ssh polishhussar@192.168.1.131 -p 2222

Link hosts:
    http://192.168.1.131:15672 - rabbit
    http://192.168.1.131:8081/ - mongo-express

Install Jenkinsa in to server develop: https://jenkins.io/doc/book/installing/

    wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -
    sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
    sudo apt-get update
    sudo apt-get install jenkins

Run jenkins service:
    sudo service jenkins start
    sudo chkconfig jenkins on

Check Jenkins service:
    systemctl status jenkins

Enter web-managment:
    http://192.168.1.131:8080/
    login: Admin
    pass: admin
RabbitMq manager:
	http://localhost:15672/
	login: guest
	pass: guest

How to trigger build with GitLab using Jenkins:
    https://github.com/jenkinsci/gitlab-plugin/wiki/Setup-Example

Start rabbitMq-managment:
    docker run -d --rm  --hostname server-rabbit --name rabbitmq -p 1567:15672 rabbitmq:3-management
    login: guest
    pass: guest

Start mongo-express
    docker run --link mongo:mongo -p 8081:8081 mongo-express

Start DB mongo:
    docker run --name mongo -v /my/own/datadir:/data/db -d mongo

Start Core app:
    docker run -d -p 8080:80 --name container_name name_image

MonogDb: 
    "ConnectionString": "mongodb://localhost:27017",
